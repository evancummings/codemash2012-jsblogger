class ArticlesController < ApplicationController
	def index
		@articles = Article.all
	end

	def show 
		@article = Article.find(params[:id])
	end

	def new 
		@article = Article.new
	end

	def create
		@article = Article.new(params[:article])
		if @article.save
			flash[:message] = "Article Created"
			redirect_to article_path(@article)
		else
			#Show them the form again
			#with the data, so you're not a jerk
			flash[:message] = "Sorry, there are errors in the form"
			render :new	
		end
	end

	def destroy
		#find the article
		article = Article.destroy(params[:id])
		flash[:message] = "Article '#{article.title} was deleted'"
		#send them back to the index
		redirect_to articles_path

	end

	def edit
		@article = Article.find(params[:id])
	end

	def update
		@article = Article.find(params[:id])
		if @article.update_attributes(params[:article])
			flash[:message] = "Article Updated"
			redirect_to article_path(@article)
		else
			flash[:message] = "Sorry, there were errors in the form"
			render :edit
		end
	end
end
